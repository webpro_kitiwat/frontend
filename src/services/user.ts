import http from "@/services/axios";
import type User from "@/types/user";
function getUser() {
  return http.get("/users");
}
function saveUser(user: User) {
  return http.post("/users", user);
}
function updateUser(id: number, user: User) {
  return http.patch("/users/" + id, user);
}
function deleteUser(id: number) {
  return http.delete("/users/" + id);
}
export default { getUser, saveUser, deleteUser, updateUser };
