import { ref, watch } from "vue";
import { defineStore } from "pinia";
import usersService from "@/services/user";
import type User from "@/types/user";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";

export const useUserStore = defineStore("user", () => {
  const users = ref<User[]>([]);
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  const dialog = ref(false);
  const editedUsers = ref<User>({ name: "", age: 0, tel: "" });
  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedUsers.value = { name: "", age: 0, tel: "" };
    }
  });
  async function getUsers() {
    try {
      const res = await usersService.getUser();
      users.value = res.data;
      console.log(res);
    } catch (e) {
      console.log(e);
    }
  }

  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUsers.value.id) {
        const res = await usersService.updateUser(
          editedUsers.value.id,
          editedUsers.value
        );
      } else {
        const res = await usersService.saveUser(editedUsers.value);
      }
      dialog.value = false;
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูลได้");
    }
    loadingStore.isLoading = false;
  }

  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await usersService.deleteUser(id);
      await getUsers();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูลได้");
    }
    loadingStore.isLoading = false;
  }

  function editUser(user: User) {
    editedUsers.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }
  return {
    users,
    getUsers,
    saveUser,
    deleteUser,
    editUser,
    dialog,
    editedUsers,
  };
});
