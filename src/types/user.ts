export default interface User {
  id?: number;
  name: string;
  age: number;
  tel: string;
  createAt?: Date;
  updatedAt?: Date;
  delAt?: Date;
}
